with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Command_Line;    use Ada.Command_Line;

procedure Intcode is
   type Memory_Array is Array(Natural range <>) of Natural;

   function Max_Position(File: in File_Type) return Natural is
      N: Natural := 0;
      C: Character;
   begin
      while not End_Of_File(File) loop
         Get(File, C);
         if C = ',' then
            N := N + 1;
         end if;
      end loop;
      return N;
   end Max_Position;

   function Load_Input(File: in out File_Type) return Memory_Array is
   -- Process the input file in two passes.
   -- 1. Determine the number of "positions".
   -- 2. Parse the initial values.
      N  : constant Natural := Max_Position(File);
      C:   Character;
      EOF: Boolean := false;
   begin
      Reset(File);
      return Result : Memory_Array(0..N) do
         for I in Result'Range loop
            Get(File, Result(I));
            
            -- Skip over commas, exit on EOF.
            Look_Ahead(File, C, EOF);
            exit when EOF;
            if C = ',' then
               Get(File, C);
            end if;
         end loop;
      end return;
   end Load_Input;

   Op_Halt : constant Natural := 99;
   Op_Add  : constant Natural := 1;
   Op_Mul  : constant Natural := 2;
   Invalid_Opcode: exception;

   procedure Run_Program(Memory: in out Memory_Array) is
      PC: Natural := 0;
      P1, P2, P3: Natural;
   begin
      while Memory(PC) /= Op_Halt loop
         -- Extract operand and result positions.
         P1 := Memory(PC + 1);
         P2 := Memory(PC + 2);
         P3 := Memory(PC + 3);

         -- Perform the requested operation.
         case Memory(PC) is
            when Op_Add => Memory(P3) := Memory(P1) + Memory(P2);
            when Op_Mul => Memory(P3) := Memory(P1) * Memory(P2);
            when others => raise Invalid_Opcode;
         end case;

         -- Skip over the operands and result.
         PC := PC + 4;
      end loop;
   end Run_Program;

   Input_Filename : constant String := (
      if Argument_Count > 0 then Argument(1) else "1202-input");
   Input_File     : File_Type;

begin

   Put("Processing input file: ");
   Put_Line(Input_Filename);
   Open(Input_File, In_File, Input_Filename);

   declare
      Memory : Memory_Array := Load_Input(Input_File);
      Pos    : Natural := 0;
   begin
      Run_Program(Memory);
      for Val of Memory loop
         Put(Pos, Width => 4);
         Put(": ");
         Put_Line(Val'Image);
         Pos := Pos + 1;
      end loop;
   end;
end Intcode;
