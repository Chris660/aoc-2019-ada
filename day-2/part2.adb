with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Command_Line;    use Ada.Command_Line;

procedure Part2 is
   type Memory_Array is Array(Natural range <>) of Natural;
   subtype Input is Natural range 0..99;

   function Max_Position(File: in File_Type) return Natural is
      N: Natural := 0;
      C: Character;
   begin
      while not End_Of_File(File) loop
         Get(File, C);
         if C = ',' then
            N := N + 1;
         end if;
      end loop;
      return N;
   end Max_Position;

   function Load_State(File: in out File_Type) return Memory_Array is
   -- Process the input file in two passes.
   -- 1. Determine the number of "positions".
   -- 2. Parse the initial values.
      N  : constant Natural := Max_Position(File);
      C:   Character;
      EOF: Boolean := false;
   begin
      Reset(File);
      return Result : Memory_Array(0..N) do
         for I in Result'Range loop
            Get(File, Result(I));
            
            -- Skip over commas, exit on EOF.
            Look_Ahead(File, C, EOF);
            exit when EOF;
            if C = ',' then
               Get(File, C);
            end if;
         end loop;
      end return;
   end Load_State;

   function Load_State(Filename: in String) return Memory_Array is
      File: File_Type;
   begin
      Open(File, In_File, Filename);
      return Result : Memory_Array := Load_State(File) do
         close(File);
      end return;
   end Load_State;

   Input_Filename : constant String := "input";
   Memory_State   : constant Memory_Array := Load_State(Input_Filename);

   Op_Halt : constant Natural := 99;
   Op_Add  : constant Natural := 1;
   Op_Mul  : constant Natural := 2;
   Invalid_Opcode: exception;

   function Eval_Program(State      : in Memory_Array;
                         Noun, Verb : in Natural) return Natural is
      Memory     : Memory_Array := State;
      PC         : Natural := 0;
      P1, P2, P3 : Natural;
   begin
      -- Set inputs.
      Memory(1) := Noun;
      Memory(2) := Verb;

      -- Execute programme.
      while Memory(PC) /= Op_Halt loop
         -- Extract operand and result positions.
         P1 := Memory(PC + 1);
         P2 := Memory(PC + 2);
         P3 := Memory(PC + 3);

         -- Perform the requested operation.
         case Memory(PC) is
            when Op_Add => Memory(P3) := Memory(P1) + Memory(P2);
            when Op_Mul => Memory(P3) := Memory(P1) * Memory(P2);
            when others => raise Invalid_Opcode;
         end case;

         -- Skip over the operands and result.
         PC := PC + 4;
      end loop;

      return Memory(0);
   end Eval_Program;

   procedure Find_Inputs(State : in Memory_Array;
                         Target: in Natural;
                         Noun  : out Input;
                         Verb  : out Input) is
   begin
      outer:
      for N in Input'Range loop
         for V in Input'Range loop
            if Eval_Program(State, N, V) = Target then
               Noun := N;
               Verb := V;
               exit outer;
            end if;
         end loop;
      end loop outer;
   end Find_Inputs;

   Target: constant Natural := (
      if Argument_Count > 0 then Natural'Value(Argument(1)) else 19690720);
   Noun, Verb: Natural := 0;
begin
   Find_Inputs(Memory_State, Target, Noun, Verb);
   Put("Noun: "); Put(Noun); New_Line;
   Put("Verb: "); Put(Verb); New_Line;
end Part2;
