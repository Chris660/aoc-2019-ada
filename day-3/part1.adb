with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Command_Line;    use Ada.Command_Line;

-- This is actually the solution for parts 1 and 2.
-- My first thoughts were to use a map/dict keyed by the grid coordinates
-- to record the occupied points.  I decided it would be more interesting
-- (and efficient) to break the lines up into segments, then determine
-- whether they intersect.
procedure Part1 is

   -- The distance of a Point from the origin.
   type Distance is new Natural;

   -- Point defined as a cartesian coordinate
   type Point is
      record
         X, Y  : Integer := 0;
         Steps : Distance := 0; -- steps taken, for part 2.
      end record;

   -- An array of points forms a line.
   type Points is array(Positive range <>) of Point;

   -- Valid directions.
   type Direction is new Character
      with Static_Predicate => Direction in 'U' | 'D' | 'L' | 'R';

   -- Return the number of Points defined in a string.
   function Count_Points(Line: in String) return Positive is
   begin
      return Result : Positive := 1 do
         for C of Line loop
            if C = ',' then
               Result := Result + 1;
            end if;
         end loop;
      end return;
   end Count_Points;

   -- Translate a point given a direction and step count.
   function Translate_Point(From : in Point;
                            Dir  : in Direction;
                            Steps: in Distance) return Point is
      Total_Steps: constant Distance := From.Steps + Steps;
   begin
      return (case Dir is
         when 'U' => (From.X, From.Y + Integer(Steps), Total_Steps),
         when 'D' => (From.X, From.Y - Integer(Steps), Total_Steps),
         when 'L' => (From.X - Integer(Steps), From.Y, Total_Steps),
         when 'R' => (From.X + Integer(Steps), From.Y, Total_Steps)
      );
   end Translate_Point;

   -- Parse a line of input.
   function Parse_Line(Line: in String) return Points is
      N    : constant Positive := Count_Points(Line);
      S, E : Positive := 1; -- start/end positions of the current command
   begin
      -- NB. one extra point at (0, 0)
      return Result: Points(1 .. N + 1) do
         -- Parse the rest of the points. 
         for I in 2..Result'Last loop
            -- Find the next comma.
            S := E;

            while E < Line'Last and then Line(E) /= ',' loop
               E := E + 1;
            end loop;

            if Line(E) = ',' then
               E := E - 1;
            end if;

            -- Apply the movement to the last known position.
            Result(I) := Translate_Point(
               Result(I - 1),
               Direction(Line(S)),
               Distance'Value(Line(S + 1 .. E))
            );
            E := E + 2; -- skip over the comma
         end loop;
      end return;
   end Parse_Line;

   function Manhattan_Distance(X, Y: Integer) return Point is
      (X, Y, Distance(abs(X) + abs(Y)));

   Procedure Bounds(A1, A2, B1, B2: in Integer;
                    Lower_Bound, Upper_Bound: out Integer) is
      -- Order the points in each segment
      A_Min: Integer := Integer'Min(A1, A2);
      A_Max: Integer := Integer'Max(A1, A2);
      B_Min: Integer := Integer'Min(B1, B2);
      B_Max: Integer := Integer'Max(B1, B2);

   begin
      -- Now find the maximum of the two mins, and
      -- the minimum of the two maximums.
      Lower_Bound := Integer'Max(A_Min, B_Min);
      Upper_Bound := Integer'Min(A_Max, B_Max);
   end Bounds;

   procedure Find_Closest_Intersection(
         Line1, Line2: in Points;
         Best_Manhattan, Best_Steps: in out Point) is
      Intersection   : Point;
      LX, UX, LY, UY : Integer;
   begin
      -- Set a low bar :-)
      Best_Steps.Steps     := Distance'Last;
      Best_Manhattan.Steps := Distance'Last;

      -- Test points of line 2 against all segments of line 1
      for I in Line1'First + 1 .. Line1'Last - 1 loop
         for J in Line2'First + 1 .. Line2'Last - 1 loop
            -- Check for X coordinate overlap.
            Bounds(Line1(I).X, Line1(I + 1).X,
                   Line2(J).X, Line2(J + 1).X,
                   LX, UX);

            -- Now the same for Y.
            Bounds(Line1(I).Y, Line1(I + 1).Y,
                   Line2(J).Y, Line2(J + 1).Y,
                   LY, UY);

            -- Iterate over all the grid coordinates that overlap.
            for X in LX .. UX loop
               for Y in LY .. UY loop
                  -- Part 1: the Manhattan distance.
                  Intersection := Manhattan_Distance(X, Y);

                  -- Store the best results so far.
                  if Intersection.Steps < Best_Manhattan.Steps then
                     Best_Manhattan := Intersection;
                  end if;

                  -- Part 2: sum the steps for both lines with the (absolute)
                  -- distance from the last Point to this intersection.
                  Intersection.Steps := Line1(I).Steps + Line2(J).Steps
                     + Distance(abs(X - Line1(I).X) + abs(Y - Line1(I).Y))
                     + Distance(abs(X - Line2(J).X) + abs(Y - Line2(J).Y));

                  if Intersection.Steps < Best_Steps.Steps then
                     Best_Steps := Intersection;
                  end if;
               end loop;
            end loop;
         end loop;
      end loop;
   end Find_Closest_Intersection;

   File_Name: constant String := (
      if Argument_Count > 0 then Argument(1) else "input");

   Input_File: File_Type;

begin
   Open(Input_File, In_File, File_Name);
   declare
      Line1: constant Points := Parse_Line(Get_Line(Input_File));
      Line2: constant Points := Parse_Line(Get_Line(Input_File));

      Best_Manhattan, Best_Steps: Point; -- The results.
   begin
      Find_Closest_Intersection(
            Line1, Line2,
            Best_Manhattan, Best_Steps);

      -- Put_Line("Line1: " & Line1'Length'Image & " points");
      -- Put_Line("Line2: " & Line2'Length'Image & " points");

      Put("Closest Manhattan: (");
      Put(Best_Manhattan.X'Image); Put(", "); Put(Best_Manhattan.Y'Image);
      Put(") - distance: ");
      Put(Best_Manhattan.Steps'Image);
      New_Line;

      Put("Closest by total steps: (");
      Put(Best_Steps.X'Image); Put(", "); Put(Best_Steps.Y'Image);
      Put(") - distance: ");
      Put(Best_Steps.Steps'Image);
      New_Line;
   end;
end Part1;
