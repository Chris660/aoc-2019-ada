with Ada.Text_IO;  use Ada.Text_IO;

procedure Part2 is
   subtype Mass is Natural;
   subtype Fuel is Natural;

   function Mass_To_Fuel(Module_Mass: in Mass) return Fuel is
   begin
      if Module_Mass < 6 then
         return 0;
      else
         return Fuel'((Module_Mass / 3) - 2);
      end if;
   end Mass_To_Fuel;

   -- Total mass for a module including its fuel.
   function Total_Fuel_Mass(Module_Mass: in Mass) return Fuel is
      Acc  : Fuel := 0;
      Last : Fuel := Module_Mass;
   begin
      while Last > 0 loop
         Last := Mass_To_Fuel(Last);
         Acc  := Acc + Last;
      end loop;
      return Acc;
   end Total_Fuel_Mass;

   Sum : Fuel := 0; -- Output accumulator

begin
   while not End_Of_File loop
      Sum := Sum + Total_Fuel_Mass(Mass'Value(Get_Line));
   end loop;
   Put_Line(Sum'Image);
end Part2;
