with Ada.Text_IO;  use Ada.Text_IO;

procedure Part1 is
   subtype Mass is Natural;
   subtype Fuel is Natural;

   function Mass_To_Fuel(Module_Mass: in Mass) return Fuel is
   begin
      return Fuel'((Module_Mass / 3) - 2);
   end Mass_To_Fuel;

   Sum  : Fuel := 0;       -- Output accumulator

begin
   while not End_Of_File loop
      Sum := Sum + Mass_To_Fuel(Mass'Value(Get_Line));
   end loop;
   Put_Line(Sum'Image);
end Part1;
