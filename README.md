My attempts at the [Advent of Code 2019][1] challenge - in [Ada][2]!

Caveat lector: I'm in the process of learning Ada, so don't expect
my solutions to be idiomatic or efficient. :-)

[1]: https://adventofcode.com/2019
[2]: https://learn.adacore.com/
