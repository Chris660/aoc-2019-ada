with Ada.Text_IO;  use Ada.Text_IO;

procedure Part1 is
   -- However, they do remember a few key facts about the password:
   --
   -- * It is a six-digit number.
   -- * The value is within the range given in your puzzle input.
   -- * Two adjacent digits are the same (like 22 in 122345).
   -- * Going from left to right, the digits never decrease; they
   --   only ever increase or stay the same (like 111123 or 135679).
   --
   -- Other than the range rule, the following are true:
   --
   -- * 111111 meets these criteria (double 11, never decreases).
   -- * 223450 does not meet these criteria (decreasing pair of digits 50).
   -- * 123789 does not meet these criteria (no double).
   --
   -- How many different passwords within the range given in your puzzle input meet these criteria?


   -- My first thought was to define:
   --
   --   type Password is range 172851 .. 675869;
   -- 
   -- But validating the rules above is going to be awkward as we
   -- need to address individual *decimal* digits.
   --
   -- I think a small array of Characters might be a reasonable
   -- model for the password.
   subtype Password_Digit is Character range '0'..'9';
   type Password_Index is range 1..6;
   type Password is array (Password_Index) of Password_Digit;

   Min: constant Password := ('1', '7', '2', '8', '5', '1');
   Max: constant Password := ('6', '7', '5', '8', '6', '9');

   function Is_Valid_Part1(PW: in Password) return Boolean is
      Found_Decrease : Boolean := false;
      Found_Pair     : Boolean := false;
   begin
      for I in Password'First + 1 .. Password'Last loop
         if PW(I) < PW(I - 1) then
            Found_Decrease := true;
            exit;
         end if;
         if PW(I) = PW(I - 1) then
            Found_Pair := true;
         end if;
      end loop;
      return Found_Pair and not Found_Decrease;
   end Is_Valid_Part1;

   function Is_Valid_Part2(PW: in Password) return Boolean is
      Run_Count      : Password_Index := 1;
      Found_Decrease : Boolean := false;
      Found_Pair     : Boolean := false;
   begin
      for I in Password'First + 1 .. Password'Last loop
         if PW(I) < PW(I - 1) then
            Found_Decrease := true;
            exit;
         end if;
         if not Found_Pair then
            if PW(I) = PW(I - 1) then
               Run_Count := Run_Count + 1;
            else
               Found_Pair := Run_Count = 2;
               Run_Count := 1;
            end if;
         end if;
      end loop;

      -- The last two chars are a pair.
      Found_Pair := Found_Pair or Run_Count = 2;
      return Found_Pair and not Found_Decrease;
   end Is_Valid_Part2;

   Overflow : exception;

   procedure Next(PW: in out Password) is
      Index: Password_Index := Password_Index'Last;
      Fill: Password_Digit;
   begin
      while PW(Index) = Password_Digit'Last loop
         if Index = Password_Index'First then
            raise Overflow;
         end if;
         Index := Index - 1;
      end loop;

      -- Increment the digit at the selected index, and
      -- right fill the less significant digits with the
      -- same value to skip obviously decreasing values.
      Fill := Password_Digit'Succ(PW(Index));
      PW(Index .. Password_Index'Last) := (others => Fill);
   end Next;
   
   Search_Count : Natural  := 0;
   Search_Pass  : Password := Min;
begin
   while Search_Pass <= Max loop
      if Is_Valid_Part1(Search_Pass) then
         Search_Count := Search_Count + 1;
      end if;
      Next(Search_Pass);
   end loop;

   Put("Part 1 found ");
   Put(Search_Count'Image);
   Put(" valid passwords.");
   New_Line;

   Search_Count := 0;
   Search_Pass  := Min;
   while Search_Pass <= Max loop
      if Is_Valid_Part2(Search_Pass) then
         Search_Count := Search_Count + 1;
      end if;
      Next(Search_Pass);
   end loop;

   Put("Part 2 found ");
   Put(Search_Count'Image);
   Put(" valid passwords.");
   New_Line;
end Part1;
